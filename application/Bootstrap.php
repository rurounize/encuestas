<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    
    /**
     * Bugfix: ZF 1.12.3 Autocargador para el modulo vacío, que hace referencia 
     *         a las carpetas dentro de Application.
     */
    protected function _initResourceAutoloader()
    {
        $resourceAutoloader = new Zend_Application_Module_Autoloader(array(
            'basePath'      => APPLICATION_PATH,
            'namespace'     => ''
        ));
        
        return $resourceAutoloader;
    }
    
}

