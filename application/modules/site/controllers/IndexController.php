<?php

class Site_IndexController extends Zend_Controller_Action
{
    /**
     *
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    private $_flashMessenger;
    
    private $_tabla = array();

    public function init()
    {
        $this->_flashMessenger = $this->_helper->FlashMessenger;
    }

    public function indexAction()
    {   
        $form = new Site_Form_Datos();
        
        if ( $this->getRequest()->isPost() ) {
            if ( $form->isValid($_POST)) {
                
                // Subida de archivos
                $foto = Site_Service_Upload_Image::procesar($form->getElement("foto"));
                $form->removeElement("foto");
                
                $doc  = Site_Service_Upload_Documento::procesar(
                                $form->getElement("curriculum")
                            );
                $form->removeElement("curriculum");
                
                $row = $this->getTabla('datos')->createRow();
                $row->setFromArray($form->getValues());
                
                $fecha_nac = new Zend_Date(strtolower($row->fecha_nacimiento));
                $row->fecha_nacimiento = $fecha_nac->toString("yyyy-MM-dd");
                
                $row->foto = $foto;
                $row->curriculum = $doc;
                
                if ( $row->correo != null ) {
                    $mail = new Site_Service_Sendmail($row->correo, $row->nombre);
                    $mail->send();
                }
                
                $row->save();
                
                // Tipo de mensaje
                $this->_flashMessenger->addMessage("success"); 
                // Mensaje
                $this->_flashMessenger->addMessage("Se a guardado correctamente.");
                
                $this->_redirect($this->view->url());
                exit;
            }
        }
        
        if ( $this->_flashMessenger->hasMessages() ) {
            $this->view->mensajes = $this->_flashMessenger->getMessages();
        }
        
        $form->setAction($this->view->url());     
        $this->view->form = $form;
    }
    
    /**
     * Llamar una tabla
     * 
     * @param string $tabla
     * @return Zend_Db_Table_Abstract
     */
    private function getTabla( $tabla ) {
        if ( !array_key_exists($tabla, $this->_tabla ) ) {
            $class = 'Model_DbTable_'.ucfirst($tabla);
            $this->_tabla[$tabla] = new $class();
        }
        return $this->_tabla[$tabla];
    }

}
