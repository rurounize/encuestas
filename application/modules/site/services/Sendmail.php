<?php

class Site_Service_Sendmail
{
    /**
     * Destinatario
     * 
     * @var array 
     */
    protected $_to = array();
    
    /**
     * Enviar correo
     * 
     * @param string $email
     * @param string $nombre
     */
    public function __construct( $email = null, $nombre = null) 
    {
        $this->_to["email"] = $email;
        $this->_to["nombre"]= $nombre;
    }
    
    public function send()
    {
        $config = array(
                'auth'     => 'login',
                'ssl'      => 'ssl',
                'username' => 'zend@tuxmapa.com.mx',
                'password' => 'zendmail1067',
                'port'     => 465
            );
 
        $transport = new Zend_Mail_Transport_Smtp('mail.tuxmapa.com.mx', $config);

        $mail = new Zend_Mail('UTF-8');
        $mail->setBodyText('Este es un correo de prueba.');
        $mail->setFrom('zend@tuxmapa.com.mx', 'Zend Mail');
        $mail->addTo($this->_to["email"], $this->_to["nombre"]);
        $mail->setSubject('Prueba');
        $mail->send($transport);
    }
}
