<?php

class Site_Service_Upload_Image
{
    /**
     * 
     * @param Zend_Form_Element_File $foto Description
     * @return string Nombre del archivo
     */
    public static function procesar( $foto )
    {
        $path = $foto->getDestination();
        $name = $foto->getFileName();
        
        // Obteniendo la extensión
        $ext = substr($name, strrpos($name, '.') + 1);
        
        $nombre = time() . "." . $ext;
        
        // Cambiar de nombre
        $foto->addFilter("Rename", array(
            "target"    => $path . "/" . $nombre,
        ));
        
        if ( ! $foto->receive() ) {
            return "";
        }
        
        // Redimencionar la imagen
        $imagen = new ZFImage_Image($path . "/" . $nombre );
        $imagen->addPlugin(new ZFImage_Fx_Resize(400));
        // Guardando la imagen
        $imagen->imageJpeg($path . "/" . $nombre);
        
        // Retornar el nombre del archivo
        return $nombre;
    }
}
