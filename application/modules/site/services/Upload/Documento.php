<?php

class Site_Service_Upload_Documento
{
    /**
     * 
     * @param Zend_Form_Element_File $curriculum Description
     * @return string Nombre del archivo
     */
    public static function procesar( $curriculum )
    {
        $path = $curriculum->getDestination();
        $name = $curriculum->getFileName();
        
        // Obteniendo la extensión
        $ext = substr($name, strrpos($name, '.') + 1);
        
        $nombre = time() . "." . $ext;
        
        // Cambiar de nombre
        $curriculum->addFilter("Rename", array(
            "target"    => $path . "/" . $nombre,
        ));
        
        if ( ! $curriculum->receive() ) {
            return "";
        }
        
        // Retornar el nombre del archivo
        return $nombre;
    }
}
