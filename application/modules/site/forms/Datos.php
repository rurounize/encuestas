<?php

class Site_Form_Datos
    extends Twitter_Form
{
    public function init()
    {
        $this->setAttrib("class", "form-horizontal");
        
        $nombre = new Zend_Form_Element_Text('nombre');
        $nombre->setLabel('Nombre')
               ->setRequired();
        $this->addElement($nombre);
        
        $apaterno = new Zend_Form_Element_Text('apellido_paterno');
        $apaterno->setLabel('Apellido Paterno')
                 ->setRequired();
        $this->addElement($apaterno);
        
        $amaterno = new Zend_Form_Element_Text('apellido_materno');
        $amaterno->setLabel('Apellido Materno');
        $this->addElement($amaterno);
        
        $fnacimiento = new Zend_Form_Element_Text('fecha_nacimiento');
        $fnacimiento->setLabel('Fecha de Nacimento')
                    ->setRequired();
        $this->addElement($fnacimiento);
        
        $genero = new Zend_Form_Element_Radio('genero');
        $genero->setLabel('Genero');
        $genero->setMultiOptions(array(
            'femenino'  => 'Femenino',
            'masculino' => 'Masculino'
        ));
        $genero->setRequired();
        $this->addElement($genero);
        
        $correo = new Zend_Form_Element_Text('correo');
        $correo->setLabel('Correo');
        $this->addElement($correo);
        
        $direccion = new Zend_Form_Element_Text('direccion');
        $direccion->setLabel('Dirección');
        $this->addElement($direccion);
        
        $escolaridad = new Zend_Form_Element_Select('escolaridad');
        $escolaridad->setLabel('Escolaridad');
        $escolaridad->setMultiOptions(
                    $this->getEscolaridad()
                );
        $this->addElement($escolaridad);
        
        $this->addElement('checkbox', 'ofimatica', array(
            'label'     => 'Ofimática',
            'description'=> '(Excel, Word, PowerPoint, etc.)',
            'multioptions'=> array(
                '1' => 'Si'
            )
        ));
        
        $this->addElement('text', 'ingles', array(
            'label'     => 'Inglés',
        ));
        
        $this->addElement('file', 'foto', array(
            'label'     => 'Foto',
            'destination' => APPLICATION_PATH . '/../public/uploads',
            'validators'  => array(
                array('Extension', false, 'jpg,png,JPG,PNG')
            )
        ));
        
        $this->addElement('file', 'curriculum', array(
            'label'     => 'Curriculum',
            'destination' => APPLICATION_PATH . '/../public/uploads',
            'validators'  => array(
                array('Extension', false, 'pdf,doc,docx,odt')
            )
        ));
        
        $this->addElement('textarea', 'comentarios', array(
            'label'     => 'Comentarios'
        ));
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Terminar');
        $this->addElement($submit);
    }
    
    private function getEscolaridad()
    {
        $tabla = new Model_DbTable_Escolaridad();
        $rows = $tabla->fetchAll();
        $data = array();
        foreach ($rows as $row ) {
            $data[$row->id] = $row->escolaridad;
        }
        return $data;
    }
}
