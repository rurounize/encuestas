<?php

class Model_Usuario
    extends Zend_Db_Table_Row_Abstract
{   
    protected $_data = array(
        "id"        => null,
        "username"  => null,
        "password"  => null,
        "password_salt" => null,
        "real_name" => null,
        "rol"       => null
    );


    public function setNewPassword( $password )
    {
        $salt = $this->_getSalt();
        $password = sha1( md5($password) . $salt);
        $this->password = $password;
        $this->password_salt = $salt;
    }
    
    public function updatePassword( $password, $new_password )
    {
        // Revisando
        $salt = $this->password_salt;
        $password = sha1( md5($password) . $salt);
        if ( !strcmp($this->password, $password) ) {
            return false;
        }
        
        $this->setNewPassword($new_password);
        return true;
    }
    
    private function _getSalt()
    {
        $alpha = "1234567890abcdefghijklmnñopqrstuvwxyz";
        $key = "";
        for ( $i = 1; $i <= 20; $i++ ) {
            $key .= $alpha[rand(0, strlen($alpha)-1)];
        }
        
        return sha1($key);
    }
    
}
