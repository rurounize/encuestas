<?php

class Model_Dato 
    extends Zend_Db_Table_Row_Abstract
{
    protected $_data = array(
        'id'        => null,
        'nombre'    => null ,
        'apellido_paterno' => null,
        'apellido_materno' => null,
        'fecha_nacimiento' => null,
        'genero'    => null,
        'correo'    => NULL,
        'direccion' => NULL,
        'escolaridad' => NULL,
        'ofimatica' => NULL,
        'ingles'    => NULL,
        'foto'      => NULL,
        'curriculum'=> NULL,
        'comentarios'=> NULL,
    );
}

