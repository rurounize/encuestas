<?php

class Model_DbTable_Usuarios
    extends Zend_Db_Table_Abstract
{
    // Nombre de la tabla
    protected $_name    = "usuarios";
    // El objeto que debe devolver
    protected $_rowClass= "Model_Usuario";
}